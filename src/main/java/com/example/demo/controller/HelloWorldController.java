package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloWorldController {
    @GetMapping("/hello")
    public String helloWorld(@RequestParam String name) {
        return "Hello " + name;
    }
}
